var React = require('react');
var VideosContainer = require('./components/videos-container')

var App = React.createClass({
  render: function () {
    return (
      <div>
        <VideosContainer />
      </div>
    )
  }
});

module.exports = App;
