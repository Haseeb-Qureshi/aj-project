'use strict';
var ActionCreator = require('../actions/action-creator');
var $ = require('jquery');
var localUrl = '/cards.json';
var corsUrl = 'http://allow-any-origin.appspot.com/https://api.ajplus.net/v2/cards?platform=tvos';

module.exports = {
  loadData: function() {
    var that = this;

    $.getJSON(localUrl, function(data) {
      var videos = that.parseAllVideoData(data);
      ActionCreator.populateAllVideoData(videos);
    });
  },

  parseAllVideoData: function(data) {
    return data['data'].reduce(function (videos, videoData) {
      var newVideo = {};

      newVideo.id = videoData.nid;
      newVideo.title = videoData.title;
      newVideo.createdAt = videoData.created;
      newVideo.description = videoData.description;
      newVideo.creator = videoData.credits[0].screen_name;
      newVideo.thumbnailUrl = videoData.styled_images[3].styled_image_url;

      videos.push(newVideo);
      return videos;
    }, []);
  },
};
