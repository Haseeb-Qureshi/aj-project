var AppDispatcher = require('../dispatcher/app-dispatcher');

module.exports = {
  populateAllVideoData: function (videos) {
    var action = {
      actionType: "POPULATE_VIDEOS",
      videoData: videos,
    };

    AppDispatcher.dispatch(action);
  }
};
