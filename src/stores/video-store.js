var AppDispatcher = require('../dispatcher/app-dispatcher');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var videos = [];

var VideoStore = assign({}, EventEmitter.prototype, {
  emitChange: function () {
    this.emit('change');
  },

  addChangeListener: function (callback) {
    this.on('change', callback);
  },

  removeChangeListener: function (callback) {
    this.removeListener('change', callback);
  },

  _populateVideos: function (videoData) {
    videos = videoData;
    console.log(videos)
    this.emitChange();
  },

  getVideos: function () {
    return videos;
  },
});

AppDispatcher.register(function (action) {
  switch (action.actionType) {
    case "POPULATE_VIDEOS":
      VideoStore._populateVideos(action.videoData);
      break;
    default:
  }
});

module.exports = VideoStore;
