var React = require('react');
var ReactDOM = require('react-dom');
var App = require('./App');
var Video = require('./components/video');
var apiUtil = require('./utils/api-util');


ReactDOM.render(
  <App />,
  document.getElementById('root')
);

apiUtil.loadData();
