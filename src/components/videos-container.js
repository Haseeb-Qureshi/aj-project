var React = require('react');
var VideoStore = require('../stores/video-store');
var Video = require('./video');

function getVideoState() {
  return { videos: VideoStore.getVideos() };
};

var VideosContainer = React.createClass({
  onChange: function () {
    this.setState(getVideoState());
  },

  getInitialState: function () {
    return getVideoState();
  },

  componentDidMount: function () {
    VideoStore.addChangeListener(this.onChange);
  },

  componentWillUnmount: function () {
    VideoStore.removeChangeListener(this.onChange);
  },

  render: function () {
    var videoChildren = this.state.videos.map(function (videoData) {
      return <Video
              key={videoData.id}
              id={videoData.id}
              title={videoData.title}
              createdAt={videoData.createdAt}
              description={videoData.description}
              creator={videoData.creator}
              thumbnailUrl={videoData.thumbnailUrl} />
    });

    return (
      <div>{videoChildren}</div>
    )
  }
});

module.exports = VideosContainer;
