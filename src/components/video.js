var React = require('react');
var PropTypes = React.PropTypes;

var Video = React.createClass({
  propTypes: {
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    creator: PropTypes.string.isRequired,
    thumbnailUrl: PropTypes.string.isRequired,
  },

  render: function () {
    return (
      <div className="video-block">
        {JSON.stringify(this.props)}
      </div>
    );
  }
});

module.exports = Video;
